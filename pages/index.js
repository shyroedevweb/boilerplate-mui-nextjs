import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import Header from '../src/components/Header/Header';
// import {  } from '@mui/material';
import { styled, ThemeProvider } from '@mui/material/styles';
import theme from '../styles/theme';

// const customTheme = createTheme({
//   palette: {
//     primary: {
//       main: '#197622',
//     }
//   }
// });

export default function Home() {
  return (
   <ThemeProvider theme={theme} >
   <Header />
   </ThemeProvider>
  )
}
