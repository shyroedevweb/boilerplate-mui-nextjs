import React from 'react';
import { Container, Stack, AppBar, MenuItem, Typography, Menu } from '@mui/material';
import HeaderLogo from '../../assets/logo.svg';
import Image from 'next/image';

const Header = () => {
    return (
        <AppBar position="static" >
        <Container>
            <Stack direction="row" alignItems="center" justifyContent="space-between" >
                <Image src={HeaderLogo} alt="imagem-logo" />
                <Menu id="menu-appbar" >
                    <MenuItem>
                        <Typography  >Products</Typography>
                    </MenuItem>
                    <MenuItem>
                        <Typography  >Pricing</Typography>
                    </MenuItem>
                    <MenuItem>
                        <Typography  >Blog</Typography>
                    </MenuItem>
                </Menu>
            </Stack>
        </Container>
        </AppBar>
    )
}

export default Header
