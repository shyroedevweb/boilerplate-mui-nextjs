import { createTheme, responsiveFontSizes } from '@mui/material/styles';
import { red } from '@mui/material/colors';

const theme = createTheme({
    palette: {
        primary: {
          main: '#197666',
        }
      }
});

theme = responsiveFontSizes(theme);

export default theme;